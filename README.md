# Tik Tak

Unbeatable tic tac toe game.

More information about this project is available at 
[https://haleyholden.com/tiktak](https://haleyholden.com/tiktak)

The majority of the code that I wrote is in the lib directory. 

A web version of the app can be played [here](https://haleyholden.github.io/Projects/tik_tak/build/web/#/)
(Note: the web version looks much better in Chrome).


