import "package:flutter/material.dart";
import 'package:tiktakapp/app_screens/home.dart';
import "./app_screens/opening_scene.dart";
import "./app_screens/game_board.dart";


void main() => runApp(new MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var routes = <String, WidgetBuilder>{
      OpeningScene.routeName: (BuildContext context) => new OpeningScene(),
      GameBoard.routeName: (BuildContext context) => new GameBoard(),
    };
    return new MaterialApp(
      title: 'Tik Tak App',
      debugShowCheckedModeBanner: false,
      home: Home(),
      routes: routes,
    );
  }
}



