import "package:flutter/material.dart";
import "./opening_scene.dart";

class Home extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.black,
        appBar: AppBar(
            backgroundColor: Colors.black45,
            title: Text("Tik Tak",
                style: TextStyle(
                    fontSize: 40,
                    color: Colors.lightGreen,
                    fontFamily: "CourierPrime",
                    fontWeight: FontWeight.w600))),
        body: Center(
            child: Container(
                decoration: BoxDecoration(
                    image: DecorationImage(
                        image: AssetImage("assets/images/stars.png"))),
                child: Column(children: <Widget>[
                  SizedBox(
                    height: 100,
                  ),
                  Expanded(
                    child: Image.asset("assets/images/robot_head.png",
                        fit: BoxFit.fitWidth),
                  ),
                  BeginButton(),
                  AboutButton()
                ]))));
  }
}

class BeginButton extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
        width: 250.0,
        height: 50.0,
        child: RaisedButton(
            color: Colors.lightGreen,
            child: Text("Begin Game",
                style: TextStyle(
                  fontSize: 20.0,
                  fontFamily: "CourierPrime",
                )),
            elevation: 4.0,
            onPressed: () =>
                Navigator.pushNamed(context, OpeningScene.routeName)));
  }
}

class AboutButton extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
        width: 250.0,
        height: 50.0,
        margin: EdgeInsets.only(top: 30.0, bottom: 40.0),
        child: RaisedButton(
            color: Colors.lightGreen,
            child: Text("About Bobby",
                style: TextStyle(
                  fontSize: 20.0,
                  fontFamily: "CourierPrime",
                )),
            elevation: 4.0,
            onPressed: () => showAlert(context)));
  }
}

void showAlert(BuildContext context) {
  var alertDialog = AlertDialog(
      backgroundColor: Colors.black45,
      titleTextStyle: TextStyle(
          color: Colors.lightGreen,
          fontSize: 40.0,
          fontFamily: "CourierPrime",
          fontStyle: FontStyle.italic),
      contentTextStyle: TextStyle(
        color: Colors.lightGreen,
        fontSize: 15.0,
        fontFamily: "CourierPrime",
      ),
      title: Text("About Bobby"),
      content: Text(
          "Bobby the robot has been on the moon since 2011, and has nothing to do but practice "
          "Tik Tak Toe. You are welcome to play with him, but be warned, Bobby has never lost."));

  showDialog(context: context, builder: (BuildContext context) => alertDialog);
}
