import 'decision.dart';

class AI {
  List<List> _field;
  String _humanChar;
  String _aiChar;
  Decision _decision;


  AI(this._field, this._humanChar, this._aiChar);

  Decision getDecision() {
    int move  = makeDecision();
    _decision = spotToDecision(move);
    return _decision;
  }

  makeDecision() {
    var arrayBoard = getArrayBoard();
    var bestScore = -2000;
    var move;
    for (var i = 0; i < 9; i++) {
      if (arrayBoard[i] == ' ') {
        arrayBoard[i] = _aiChar;
        var score = minimax(arrayBoard, false);
        arrayBoard[i] = ' ';
        if (score > bestScore) {
          bestScore = score;
          move = i;
        }
      }
    }
    return move;
  }

  Decision spotToDecision(int spot) {
    int j = spot % 3;
    int i = (spot - j) ~/ 3;
    return Decision(i, j);
  }

  List<String> getArrayBoard() {
    // Returns 1x9 vector form of 3x3 matrix
    List<String> arrayBoard = [];
    for (var i = 0; i < 3; i++) {
      for (var j = 0; j < 3; j++) {
        arrayBoard.add(_field[i][j]);
      }
    }
    return arrayBoard;
  }


  int minimax(List<String> board, bool maximize) {
    if (winning(board, _aiChar)) { // Check if terminal state is reached
      return 10;
    } else if (winning(board, _humanChar)) {
      return -10;
    } else if (checkDraw(board)) {
      return 0;
    }
    if (maximize) { // Theoretical ai's turn
      var bestScore = -2000;
      for (var i = 0; i < 9; i++) {
        if (board[i] == ' ') {
          board[i] = _aiChar;
          var score = minimax(board, false);
          board[i] = ' ';
          if (score > bestScore) {
            bestScore = score;
          }
        }
      }
      return bestScore;
    } else { // Theoretical human's turn
      var bestScore = 2000;
      for (var i = 0; i < 9; i++) {
        if (board[i] == ' ') {
          board[i] = _humanChar;
          var score = minimax(board, true);
          board[i] = ' ';
          if (score < bestScore) {
            bestScore = score;
          }
        }
      }
      return bestScore;
    }
  }
}

// winning combinations using the board indices for instance the first win could be 3 xes in a row
bool checkDraw(board) {
  bool draw = true;
  for (var i = 0; i < 9; i++) {
    if (board[i] == ' ') draw = false;
  }

  return draw;
}

bool winning(board, player) {
  if ((board[0] == player && board[1] == player && board[2] == player) ||
      (board[3] == player && board[4] == player && board[5] == player) ||
      (board[6] == player && board[7] == player && board[8] == player) ||
      (board[0] == player && board[3] == player && board[6] == player) ||
      (board[1] == player && board[4] == player && board[7] == player) ||
      (board[2] == player && board[5] == player && board[8] == player) ||
      (board[0] == player && board[4] == player && board[8] == player) ||
      (board[2] == player && board[4] == player && board[6] == player)) {
    return true;
  } else {
    return false;
  }
}
