import "package:flutter/material.dart";
import "./game_board.dart";
import 'package:video_player/video_player.dart';

class OpeningScene extends StatefulWidget {
  static const String routeName = "/OpeningScene";

  @override
  VideoState createState() => VideoState();
}

class VideoState extends State<OpeningScene> {
  VideoPlayerController playerController;
  VoidCallback listener;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.black45,
        appBar: AppBar(
          backgroundColor: Colors.black45,
          title: Text("Tik Tak",
              style: TextStyle(
                  fontSize: 40,
                  color: Colors.lightGreen,
                  fontFamily: "CourierPrime",
                  fontWeight: FontWeight.w600)),
        ),
        body: Container(
            decoration: BoxDecoration(
                image: DecorationImage(
                    image: AssetImage("assets/images/stars.png"))),
            child: Column(children: <Widget>[
              Expanded(
                  flex: 10,
                  child: GestureDetector(
                    onTap: () {Navigator.pushNamed(
                        context, GameBoard.routeName);},
                  child: AspectRatio(
                      aspectRatio: 12 / 16,
                      child: Container(
                          child: playerController != null
                              ? VideoPlayer(
                                  playerController,
                                )
                              : Container())))),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  RaisedButton(
                      color: Colors.lightGreen,
                      child: Icon(Icons.skip_next),
                      onPressed: () {
                        Navigator.pushNamed(
                            context, GameBoard.routeName);
                      })
                ],
              )
            ])));
  }

  @override
  void initState() {
    super.initState();
    listener = () {
      setState(() {});
    };
    startVideo();
  }

  Future<void> startVideo() async {
    createVideo();

  }

  void createVideo() {
    if (playerController == null) {
      playerController =
          VideoPlayerController.asset("assets/videos/robot_video.mp4")
            ..addListener(listener)
            ..setVolume(0)
            ..initialize()
            ..play();
    }
  }

  @override
  void dispose() {
    // Ensure disposing of the VideoPlayerController to free up resources.
    playerController.dispose();
    super.dispose();
  }
}
