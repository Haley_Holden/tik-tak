import 'dart:async';
import "package:flutter/material.dart";
import "ai.dart";

class GameBoard extends StatefulWidget {
  static const String routeName = "/GameBoard";

  @override
  State<StatefulWidget> createState() {
    return GameBoardState();
  }
}

class GameBoardState extends State<GameBoard> {
  double humanBottom,
      humanRight,
      robotTop,
      robotLeft; // These control positions
  String humanChar = 'O';
  String robotChar = 'X';
  bool humanGoesFirst = false;
  bool humanTurn;
  AI robot;
  Map positions = getPositions();
  List<List> matrix;

  GameBoardState() {
    initMatrix(humanGoesFirst);
  }

  initMatrix(bool humanGoesFirst) {
    matrix = List<List>(3);
    for (var i = 0; i < matrix.length; i++) {
      matrix[i] = List(3);
      for (var j = 0; j < matrix[i].length; j++) {
        matrix[i][j] = ' ';
      }
    }
    humanTurn = humanGoesFirst;
    robot = AI(matrix, humanChar, robotChar);
    humanBottom = positions['humanBottomReset'];
    humanRight = positions['humanRightReset'];
    robotTop = positions['robotTopReset'];
    robotLeft = positions['robotLeftReset'];

    Future.delayed(Duration(milliseconds: 100), () async {
      double width = MediaQuery.of(context).size.width;
      double height = MediaQuery.of(context).size.height;
      openingAlert(humanGoesFirst, context);

      if (!humanTurn) {
        _displayRobotTurn(width, height);
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;

    return Scaffold(
        appBar: AppBar(
            backgroundColor: Colors.black,
            title: Text("Tik Tak",
                style: TextStyle(
                    fontSize: 40,
                    color: Colors.lightGreen,
                    fontFamily: "CourierPrime",
                    fontWeight: FontWeight.w600))),
        body: Container(
          decoration: BoxDecoration(
              image: DecorationImage(
                  fit: BoxFit.fitHeight,
                  image: AssetImage("assets/images/table_top_view.jpg"))),
          child: Center(
              child: Stack(children: <Widget>[
            Positioned(
                left: (width - 300.0) / 2,
                bottom: (height - 50.0 - 300.0) / 2,
                child: Column(children: <Widget>[
                  Row(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      _buildElement(0, 0, width, height),
                      _buildElement(0, 1, width, height),
                      _buildElement(0, 2, width, height),
                    ],
                  ),
                  Row(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      _buildElement(1, 0, width, height),
                      _buildElement(1, 1, width, height),
                      _buildElement(1, 2, width, height),
                    ],
                  ),
                  Row(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      _buildElement(2, 0, width, height),
                      _buildElement(2, 1, width, height),
                      _buildElement(2, 2, width, height),
                    ],
                  )
                ])),
            AnimatedPositioned(
                left: robotLeft,
                top: robotTop,
                duration: Duration(milliseconds: 100),
                curve: Curves.ease,
                child: Container(
                  child: Container(child: RobotHand()),
                )),
            AnimatedPositioned(
                bottom: humanBottom,
                right: humanRight,
                duration: Duration(milliseconds: 100),
                curve: Curves.ease,
                child: Container(
                  child: Container(child: HumanHand()),
                )),
          ])),
        ));
  }

  _buildElement(int i, int j, double width, double height) {
    return GestureDetector(
        onTap: () {
          if (humanTurn && matrix[i][j] == ' ') {
            _displayHumanTurn(i, j, width, height);
            humanTurn = false;
            if (!_checkFinished(i, j)) {
              if (!_checkDraw()) {
                _displayRobotTurn(width, height);
              }
            }
          }
        },
        child: Container(
          width: 100.0,
          height: 100.0,
          decoration: BoxDecoration(
              color: Colors.white,
              shape: BoxShape.rectangle,
              border: Border.all(color: Colors.black)),
          child: Center(
              child: Text(
            matrix[i][j],
            style: TextStyle(fontSize: 90.0),
          )),
        ));
  }

  _resetHands() {
    setState(() {
      robotTop = positions['robotTopReset'];
      robotLeft = positions['robotLeftReset'];
    });
  }

  _displayHumanTurn(int i, int j, double width, double height) {
    setState(() {
      if (matrix[i][j] == ' ' && humanTurn) {
        // Square has to be empty
        _setHumanHands(i, j, width, height);
        matrix[i][j] = humanChar;
      }
    });
  }

  _setHumanHands(int i, int j, double width, double height) {
    humanBottom = -350 + (height - 450) / 2 + (100 * (2 - i));
    humanRight = -45 + (width - 600) / 2 + (100 * (2 - j));
  }

  _displayRobotTurn(double width, double height) {
    Timer(Duration(milliseconds: 300), () {
      setState(() {
        var decision = robot.getDecision();
        matrix[decision.row][decision.column] = robotChar;
        _setRobotHand(decision.row, decision.column, width, height);
        _checkFinished(decision.row, decision.column);
        _checkDraw();
      });
    });
    Future.delayed(Duration(seconds: 1), () {
      _resetHands();
      humanTurn = true;
    });
  }

  _setRobotHand(int i, int j, double width, double height) {
    humanRight = positions['humanRightReset'];
    humanBottom = positions['humanBottomReset'];
    robotTop = (height - 600) / 2 - 350.0 + (100 * i);
    robotLeft = (width - 600) / 2 + (100 * j);
  }

  _checkDraw() {
    var draw = true;
    matrix.forEach((i) {
      i.forEach((j) {
        if (j == ' ') draw = false;
      });
    });
    if (draw) {
      humanTurn = false;
      Future.delayed(Duration(milliseconds: 400), () {
        _showWinner(null);
      });
    }
    return draw;
  }

  _checkFinished(int x, int y) {
    var col = 0, row = 0, diag = 0, rdiag = 0;
    var n = matrix.length - 1;
    var player = matrix[x][y];

    for (int i = 0; i < matrix.length; i++) {
      if (matrix[x][i] == player) col++;
      if (matrix[i][y] == player) row++;
      if (matrix[i][i] == player) diag++;
      if (matrix[i][n - i] == player) rdiag++;
    }
    if (row == n + 1 || col == n + 1 || diag == n + 1 || rdiag == n + 1) {
      humanTurn = false;
      Future.delayed(Duration(milliseconds: 400), () {
        _showWinner(matrix[x][y]);
      });
      return true;
    }
    return false;
  }

  _showWinner(String winner) {
    String dialogText;
    if (winner == null) {
      dialogText = 'It\'s a Draw!';
    } else if (winner == humanChar) {
      dialogText = 'You won!';
    } else {
      dialogText = 'Bobby Won!';
    }

    showDialog(
        context: context,
        barrierDismissible: false,
        builder: (context) {
          return AlertDialog(
            backgroundColor: Colors.black,
            contentTextStyle: TextStyle(
                fontSize: 20,
                color: Colors.lightGreen,
                fontFamily: "CourierPrime",
                fontWeight: FontWeight.w600),
            content: Text(dialogText),
            actions: <Widget>[
              FlatButton(
                color: Colors.lightGreen,
                child: Text('Try again',
                    style: TextStyle(
                        fontSize: 20.0,
                        fontFamily: "CourierPrime",
                        color: Colors.black)),
                onPressed: () {
                  Navigator.of(context).pop();
                  setState(() {
                    humanGoesFirst = !humanGoesFirst;
                    initMatrix(humanGoesFirst); // Switch off who goes first
                  });
                },
              )
            ],
          );
        });
  }
}

void openingAlert(bool humanGoesFirst, BuildContext context) {
  String dialogText, buttonText;
  if (humanGoesFirst) {
    dialogText = "You can go first";
    buttonText = "Start";
  } else {
    dialogText = "Bobby will go first";
    buttonText = "Okay";
  }

  showDialog(
      context: context,
      builder: (context) {
        return AlertDialog(
            backgroundColor: Colors.black,
            contentTextStyle: TextStyle(
                fontSize: 20,
                color: Colors.lightGreen,
                fontFamily: "CourierPrime",
                fontWeight: FontWeight.w600),
            content: Text(dialogText),
            actions: <Widget>[
              FlatButton(
                  color: Colors.lightGreen,
                  child: Text(buttonText,
                      style: TextStyle(
                          fontSize: 20.0,
                          fontFamily: "CourierPrime",
                          color: Colors.black)),
                  onPressed: () {
                    Navigator.pop(context);
                  })
            ]);
      });
}

class HumanHand extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return (Container(
      height: 500,
      width: 250,
      decoration: BoxDecoration(
          image: DecorationImage(
              fit: BoxFit.fitHeight,
              image: AssetImage("assets/images/pencil.png"))),
    ));
  }
}

class RobotHand extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return (Container(
      height: 500,
      width: 250,
      decoration: BoxDecoration(
          image: DecorationImage(
              fit: BoxFit.fitHeight,
              image: AssetImage("assets/images/robot_pencil.png"))),
    ));
  }
}

Map getPositions() {
  var positions = Map();
  positions['robotTopReset'] = -350.0;
  positions['robotLeftReset'] = -45.0;
  positions['humanBottomReset'] = -350.0;
  positions['humanRightReset'] = -45.0;
  return positions;
}
